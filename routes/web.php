<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// Route::get('/', [
//     HomeController::class, 'index'
// ])->name('index');

// Route::get('/home', [
//     HomeController::class, 'index'
// ])->name('home.index');

// Route::get('/home/descripcion', [
//     HomeController::class, 'descripcion'
// ])->name('home.descripcion');

// Route::get('/home/contacto', [
//     HomeController::class, 'contacto'
// ])->name('home.contacto');

// Route::get('/home/clientes', [
//     HomeController::class, 'clientes'
// ])->name('home.clientes');

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('/home/descripcion', 'descripcion')->name('home.descripcion');
    Route::get('/home/contacto', 'contacto')->name('home.contacto');
    Route::get('/home/clientes', 'clientes')->name('home.clientes');
});

Route::resource('cliente',ClienteController::class);
