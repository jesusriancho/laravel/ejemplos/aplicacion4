<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo', 'Alpe')</title>
    @vite(['resources/css/app.scss'])
</head>

<body>
    <div class="container">
        @section('cabecera')
            @include('home._menu')
        @show

        @yield('contenido')
        <div class="row">
            <div class="col-lg-6">Ramon</div>
            <div class="col-lg-6">Alpe</div>
        </div>
    </div>

</body>
@vite(['resources/js/app.js'])

</html>
