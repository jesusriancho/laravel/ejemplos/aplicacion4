<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function contacto()
    {
        return view('home.contacto');
    }

    public function descripcion()
    {
        return view('home.descripcion');
    }

    public function clientes()
    {
        // muestre en un listado los clientes de la empresa
        // necesito que me creeis una migracion para crear una tabla de clientes
        // id,nombre,telefono,mail
        // con un seeder meter dos 3 clientes
        // recuperar los registros de clientes sin modelo
        $clientes = DB::table('clientes')->get();
        dd($clientes);
        return view('home.clientes', [
            'clientes' => $clientes
        ]);
    }
}
